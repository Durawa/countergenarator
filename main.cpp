
#include <iostream>


template <typename T>
class Counter
{
private:
    T startPoint{};
    T endPoint{};
    T step = static_cast<T>(1);
    T value{};
    bool isUp{ true };

public:
    explicit Counter(T endPoint);
    Counter(T startPoint, T endPoint, T step = static_cast<T>(1), bool isUp = true);
    void goStep();
    void setDirectionUp(bool isUp) { this->isUp = isUp; }
    void setCounter(T startPoint, T endPoint, T step = static_cast<T>(1), bool isUp = true);
    T getStartPoint() { return startPoint; }
    T getEndPoint() { return endPoint;  }
    T getStep() { return step; }
    T getValue() { return value; }
    bool isDirectionUp() { return isUp; }
};

template <typename T> 
Counter<T>::Counter(T endPoint) : endPoint(endPoint) {}

template <typename T>
Counter<T>::Counter(T startPoint, T endPoint, T step, bool isUp) : startPoint(startPoint), endPoint(endPoint), step(step), value(startPoint), isUp(isUp) {}

template <typename T>
void Counter<T>::goStep() {
    if (isUp) {
        value += step;
        if (value > endPoint) {
            value = value - endPoint + startPoint;
        }
    }
    else {
        value -= step;
        if (value < startPoint) {
            value = endPoint - (startPoint - value);
        }
    }
}

template <typename T>
void Counter<T>::setCounter(T startPoint, T endPoint, T step, bool isUp) {
    this->startPoint = startPoint;
    this->endPoint = endPoint;
    this->step = step;
    this->value = startPoint;
    this->isUp = isUp;
}

template <>
Counter<std::string>::Counter(std::string endPoint) = delete;

template <>
Counter<std::string>::Counter(std::string startPoint, std::string endPoint, std::string step, bool isUp) = delete;



int main()
{
    Counter<int> counter1{11};
    Counter<int> counter2{ 5, 15, 4 };
    Counter<int> counter3{ 10, 33, 4, false };



    std::cout << "\nCounter1: \n";
    for (int i = 0; i < 15; ++i)
    {
        counter1.goStep();
        std::cout << counter1.getValue() << "  ";
    }

    std::cout << "\n\nCounter2: \n";
    for (int i = 0; i < 15; ++i)
    {
        counter2.goStep();
        std::cout << counter2.getValue() << "  ";
    }

    std::cout << "\n\nCounter3: \n";
    for (int i = 0; i < 15; ++i)
    {
        counter3.goStep();
        std::cout << counter3.getValue() << "  ";
    }
    std::cout << std::endl << std::endl;

    



    auto generateIntCounter = [](int start, int end, int step, bool isUp)->Counter<int> {
        return Counter<int>(start, end, step, isUp);
    };

    auto generateDoubleCounter = [](double start, double end, double step, bool isUp)->Counter<double> {
        return Counter<double>(start, end, step, isUp);
    };


    auto counter4 = generateDoubleCounter(-4.0, 2.3, 0.5, true);

    std::cout << "\n\nCounter4: \n";
    for (int i = 0; i < 15; ++i)
    {
        counter4.goStep();
        std::cout << counter4.getValue() << "  ";
    }
    std::cout << std::endl << std::endl;


    return 0;
}
